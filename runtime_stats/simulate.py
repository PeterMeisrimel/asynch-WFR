#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 24 15:56:25 2019

@author: Peter Mesirimel, Lund University
"""

from run_tolerances import run_tolerances
from postprocessing_cpp_output import process_output_errlog
from produce_plotting_data import produce_plotting_data
import subprocess
import json
    
if __name__ == '__main__':
    print('Reading inputs...')
    import sys
    input_file = sys.argv[1] # 0-th is filename
    print('Input file: ', input_file)
    
    try:
        with open(input_file, 'r') as myfile:
            parameters_load = json.load(myfile)
    except:
        raise ValueError('invalid filename, check spelling')
        
    print('Input file loaded...')
    
    '''
    parameters need to be sorted in the following categories:
        manadatory inputs for run_tolerances
        list based inputs
        non manadatory, non list based inputs
    '''
    mandatory_keys = ['tolerances', 'run_names', 'runmodes', 'folder', 'exe', 'run_prefix', 'errorlogging', 'times', 'ref_run_name']
    mandatory_inputs = {key:value for key, value in parameters_load.items() if key in mandatory_keys}
    for k in mandatory_inputs.keys(): parameters_load.pop(k)
    
    list_parameters = {key:value for key, value in parameters_load.items() if type(value) is list}
    for k in list_parameters.keys(): parameters_load.pop(k)
    
    ## non manadatory, non list based inputs = whatever is left
    
    cropped_name = input_file[input_file.find('/') + 1:] ## skipping input_file folder
    path = run_tolerances(name = cropped_name, **mandatory_inputs, parameters = parameters_load, **list_parameters)
    print('...processing output')
    process_output_errlog(path)
    print('...producing plotting data')
    produce_plotting_data(path)
    print('...plotting')
    subprocess.call('python3 plotting.py {}'.format(path), shell = True)
    print('...done')