#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 13 12:27:08 2021

@author: Peter Meisrimel, Lund University
"""

## script for producing the iteration counter plots in the euler problem, copy over to output/euler/ and then run
n = 50 ## number of time-windows used

import numpy as np
import pylab as pl
pl.close('all')

pl.rcParams['lines.linewidth'] = 3
pl.rcParams['font.size'] = 16
pl.rcParams['ytick.labelsize'] = 14

temp_tip_final = [900]
updates = {}
for i in range(n):
    data = 0
    with open(str(i) + "/NEW_1e-10.txt") as file:
        data = file.read()
        
    temp_tip_final.append(float(data.split('\n')[-3].split(' ')[3]))
    
    updates_list = []
    data = data.split('\n')
    for d in data:
        try:
            if d.split(' ')[1] == 'update':
                updates_list.append(float(d.split(' ')[2]))
        except:
            pass
    updates_list = updates_list[::2]
    print(i, updates_list)
    updates[i] = updates_list.copy()
    
tt = np.linspace(0, 5, n+1)

pl.figure()
pl.plot(tt, temp_tip_final, marker = 'o', markersize = '8')
pl.xlabel('t ', labelpad = -30, position = (1.07, -1.2), fontsize = 20)
pl.ylabel('Temp', labelpad = -10, rotation = 0, position = (1.9, 1.), fontsize = 20)
pl.grid(which = 'major')
pl.savefig('temp_over_time_tf_5.png', dpi = 100)

import itertools
markers = itertools.cycle('oxd')
ls = itertools.cycle(['-', '--', ':'])

pl.figure()
for i in range(3):
    pl.semilogy(range(1, len(updates[i]) + 1), updates[i], label = str(i + 1), marker = next(markers), markersize = 12, linestyle = next(ls))
    up = np.array(updates[i])
    x = np.array(up)
    conv_rates = x[1:]/x[:-1]
    print(conv_rates)
pl.grid('on', 'major')
pl.legend()
pl.xlabel('k', labelpad = -30, position = (1.07, -1.2), fontsize = 18)
pl.ylabel('update', labelpad = -20, rotation = 0, position = (1.9, 1.), fontsize = 18)
pl.savefig('awr_updates.png', dpi = 100)

