#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  8 16:09:23 2019

@author: Peter Mesirimel, Lund University
"""

from __future__ import division
import matplotlib.pylab as pl
import json

pl.close('all')
pl.rcParams['lines.linewidth'] = 2
pl.rcParams['font.size'] = 18
pl.rcParams['lines.markersize'] = 9

## markers and facecolors to be used in the plots
import itertools
def reset_markers():
    return itertools.cycle('v^oD*s'), itertools.cycle('gbrckmy'), itertools.cycle(['-', '--', ':', '-.'])
def reset_fc():
    return itertools.cycle(['green', 'blue', 'red', 'cyan', 'black', 'magenta', 'yellow'])

import sys
path = sys.argv[1] # 0-th is filename

## enable plotting of variances of the errors and runtimes
# option to deactivate 
plot_variances = True
if len(sys.argv) == 3:
    if sys.argv[2] == 'False':
        plot_variances = False

## find correct plotting data files
data = {}
with open(path + 'plotting_data.txt', 'r') as myfile:
    data = json.load(myfile)
## find parameter file
parameters = {}
with open(path + 'parameters.txt', 'r') as myfile:
    parameters = json.load(myfile)
    
## create necessary directories
import os
plot_path = 'plots_' + path[path.find('/') + 1:-1]
dirs = [plot_path, plot_path + '/eps', plot_path + '/png']
for d in dirs:
    if not os.path.exists(d):
        os.makedirs(d)
        
from shutil import copy2
copy2(path + 'parameters.txt', plot_path + '/eps')
copy2(path + 'parameters.txt', plot_path + '/png')
        
fig, ax = pl.subplots()
markers, colors, linestyles = reset_markers()

for num, k in enumerate(data.keys()):
    c, m, ls = next(colors), next(markers), next(linestyles)
    pl.semilogx(data[k]['tols'], data[k]['iters_med'], label = data[k]['label'], marker = m, color = c, linestyle = ls)
    if plot_variances and ('NEW' in k):
        pl.semilogx(data[k]['tols'], data[k]['iters_min'], color = c)
        pl.semilogx(data[k]['tols'], data[k]['iters_max'], color = c)
        
#ax.set_title('Tolerance vs. #Waveform iterations', fontsize = fs + 2)
ax.set_xlabel('TOL', labelpad = -20, position = (1.05, -1), fontsize = 20)
ax.set_ylabel('k', rotation = 0, labelpad = -40, position = (2., 1.05), fontsize = 20)
ax.grid(b = True, which = 'major')
ax.tick_params(labelsize = 20)
ax.legend(loc = 0)
fig.savefig(plot_path + '/eps/tolerance_vs_iters.eps', dpi = 100)
fig.savefig(plot_path + '/png/tolerance_vs_iters.png', dpi = 100)
###########################################################

fig, ax = pl.subplots()
markers, colors, linestyles = reset_markers()

pl.loglog([float(x) for x in parameters['tolerances']],
          [float(x) for x in parameters['tolerances']],
          label = 'TOL', marker = None, color = 'k', linestyle = '--')
for num, k in enumerate(data.keys()):
    c, m, ls = next(colors), next(markers), next(linestyles)
    pl.semilogx(data[k]['tols'], data[k]['errors2_med'], label = data[k]['label'], marker = m, color = c, linestyle = ls)
    if plot_variances and ('NEW' in k):
        pl.semilogx(data[k]['tols'], data[k]['errors2_min'], color = c)
        pl.semilogx(data[k]['tols'], data[k]['errors2_max'], color = c)
        
#ax.set_title('Tolerance vs. Error', fontsize = fs + 2)
ax.set_xlabel('TOL', labelpad = -20, position = (1.08, -1), fontsize = 20)
ax.set_ylabel('Err', rotation = 0, labelpad = -50, position = (2., 1.05), fontsize = 20)
ax.grid(b = True, which = 'major')
ax.tick_params(labelsize = 20)
ax.legend(loc = 0)
fig.savefig(plot_path + '/eps/tolerance_vs_err.eps', dpi = 100)
fig.savefig(plot_path + '/png/tolerance_vs_err.png', dpi = 100)
###########################################################

fig, ax = pl.subplots()
markers, colors, linestyles = reset_markers()

for num, k in enumerate(data.keys()):
    c, m, ls = next(colors), next(markers), next(linestyles)
    pl.loglog(data[k]['tols'], data[k]['times_med'], label = data[k]['label'], marker = m, color = c, linestyle = ls)
    if plot_variances and ('NEW' in k):
        pl.loglog(data[k]['tols'], data[k]['times_min'], color = c)
        pl.loglog(data[k]['tols'], data[k]['times_max'], color = c)
    
#ax.set_title('Tolerance vs. Computational time', fontsize = fs + 2)
ax.set_xlabel('TOL', labelpad = -20, position = (1.05, -1), fontsize = 20)
ax.set_ylabel('Time', rotation = 0, labelpad = -70, position = (2., 1.05), fontsize = 20)
ax.grid(b = True, which = 'major')
ax.tick_params(labelsize = 20)
ax.legend(loc = 0)
fig.savefig(plot_path + '/eps/tolerance_vs_time.eps', dpi = 100)
fig.savefig(plot_path + '/png/tolerance_vs_time.png', dpi = 100)

###########################################################
fig, ax = pl.subplots()
markers, colors, linestyles = reset_markers()
facecolor = reset_fc()

for num, k in enumerate(data.keys()):
    c, m, f, ls = next(colors), next(markers), next(facecolor), next(linestyles)
    ax.loglog(data[k]['times_med'], data[k]['errors2_med'], label = data[k]['label'], marker = m, color = c, linestyle = ls)
    if plot_variances and ('NEW' in k):
        for n, t in enumerate(data[k]['times']):
            val_t = data[k]['times_med'][n]
            min_t = data[k]['times_min'][n]
            max_t = data[k]['times_max'][n]
            
            val_e = data[k]['errors2_med'][n]
            min_e = data[k]['errors2_min'][n]
            max_e = data[k]['errors2_max'][n]
            ax.fill_between([min_t, val_t, max_t], [val_e, max_e, val_e], [val_e, min_e, val_e], facecolor=f, interpolate=True, alpha = 0.3)
    
ax.set_xlabel('Time', labelpad = -20, position = (1.05, -1), fontsize = 20)
ax.set_ylabel('Err', rotation = 0, labelpad = -50, position = (2., 1.05), fontsize = 20)
ax.grid(b = True, which = 'major')
ax.tick_params(labelsize = 20)
ax.legend(loc = 0)
fig.savefig(plot_path + '/eps/err_vs_time.eps', dpi = 100)
fig.savefig(plot_path + '/png/err_vs_time.png', dpi = 100)

###########################################################
fig, ax = pl.subplots()
markers, colors, linestyles = reset_markers()
facecolor = reset_fc()

for num, k in enumerate(data.keys()):
    c, m, f, ls = next(colors), next(markers), next(facecolor), next(linestyles)
    pl.semilogy(data[k]['iters_med'], data[k]['errors2_med'], label = data[k]['label'], marker = m, color = c, linestyle = ls)
    if plot_variances and ('NEW' in k):
        for n, t in enumerate(data[k]['iters_med']):
            val_e = data[k]['errors2_med'][n]
            min_e = data[k]['errors2_min'][n]
            max_e = data[k]['errors2_max'][n]
            
            val_i = data[k]['iters_med'][n]
            min_i = data[k]['iters_min'][n]
            max_i = data[k]['iters_max'][n]
            ax.fill_between([min_i, val_i, max_i], [val_e, max_e, val_e], [val_e, min_e, val_e], facecolor = f, interpolate=True, alpha = 0.3)
        
#ax.set_title('Error vs. Iterations')
ax.set_xlabel('k', labelpad = -20, position = (1.08, -1), fontsize = 20)
ax.set_ylabel('Err', rotation = 0, labelpad = -50, position = (2., 1.05), fontsize = 20)
ax.grid(b = True, which = 'major')
ax.tick_params(labelsize = 20)
ax.legend(loc = 0)
fig.savefig(plot_path + '/eps/err_vs_iter.eps', dpi = 100)
fig.savefig(plot_path + '/png/err_vs_iter.png', dpi = 100)


######################################################
## error over iteration, if available

try:
    data_new = {}
    with open(path + 'results_error_iter.txt', 'r') as myfile:
        data_new = json.load(myfile)
        
    for i in ['1', '2']:
        fig, ax = pl.subplots()
        markers, colors, linestyles = reset_markers()
        facecolor = reset_fc()
        
        for num, k in enumerate(data_new.keys()):
            c, m, f, ls = next(colors), next(markers), next(facecolor), next(linestyles)
            for j, sol in enumerate(data_new[k][i]):
                pl.semilogy(range(1,len(sol+1)), sol, label = data[k]['label'], marker = m, color = c, linestyle = ls)
                break ## only showing a single one in case of multiple runs, can be adjusted here
                
        ax.set_xlabel('k', labelpad = -20, position = (1.08, -1), fontsize = 20)
        ax.set_ylabel('Err', rotation = 0, labelpad = -50, position = (2., 1.05), fontsize = 20)
        ax.grid(b = True, which = 'major')
        ax.tick_params(labelsize = 20)
        ax.legend(loc = 0)
        fig.savefig(plot_path + '/eps/err_vs_iter_single_{}.eps'.format(i), dpi = 100)
        fig.savefig(plot_path + '/png/err_vs_iter_single_{}.png'.format(i), dpi = 100)
except:
    pass