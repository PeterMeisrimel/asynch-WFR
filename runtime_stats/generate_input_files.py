#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 18 16:08:36 2021

@author: Peter Meisrimel, Lund University
"""

import json

## creation of various input "json" files for usage in simulate.py
## basic structure of an input "json"/dictionary:
## all parameters are either lists (of same length) or "simple" parameters
## the "simple" parameters are fixed in all runs, the lists are iterated through, all at the same time, no nesting/recursion

def generate_heat_DN_input_files(input_file_folder):
    from heat_relax_aux import get_parameters, get_relax_parameters
    ## file setup
    file_setup = {'folder': 'heat_DN', 'exe': 'heat_DN'}
    ## materials setup
    material_cases = ['air_water', 'air_steel', 'water_steel']
    ## problem setup
    prob_setup = {'tend': 10000, 'u0': 2}
    
    ## WR setup
    WR_setup = {'times_only_new': 1, ## runtime averaging only for new method
                'nsteps_conv_check': 1, ## steps below tolerance for convergence check
                'var_relax': 1,
                'runmodes': ['GS1', 'GS2', 'JAC', 'NEW'],
                'run_names': ['GS_DN', 'GS_ND', 'JAC', 'NEW'],
                'ref_run_name': 'GS_DN',
                'labels': ['GS_DN', 'GS_ND', 'JAC', 'NEW'],
                'maxiter' : 100,
                'errlog': 1,
                'errorlogging': 1,
                'times': 3
                }
    
    ## test cases setup
    test_case_setup = {'tolerances': [10**(-i) for i in range(12)],
                       'macrosteps': 1}
    
    basic_setup = {**file_setup, **prob_setup, **WR_setup, **test_case_setup}
    
    ## solver setup and options
    low_res = {'timesteps1': 6, 'timesteps2': 6, 'gridsize': 16} ## functionality testing 
    medium_res = {'timesteps1': 100, 'timesteps2': 100, 'gridsize': 256}
    high_res = {'timesteps1': 200, 'timesteps2': 200, 'gridsize': 512}
    
    
    fenics = {'solver1': 0, 'solver2': 0, **high_res} ## C++
    dune = {'solver1': 2, 'solver2': 2, **high_res}
    mix = {'solver1': 1, 'solver2': 2, **medium_res}
    testing = {'solver1': 1, 'solver2': 1, **low_res}
    
    solver_list = [fenics, dune, mix, testing]
    solver_names = ['fenics', 'dune', 'mix', 'testing']
    
    ## iterate over materials
    for which_material in material_cases:
        para_material = get_parameters(which_material)
        ## iterate over solvers
        for solver_case, name in zip(solver_list, solver_names):
            relax = get_relax_parameters(**para_material, **solver_case, **prob_setup)
            with open(input_file_folder + '/' + name + '_heat_DN_' + which_material, 'w') as myfile:
                parameters = {**basic_setup, **solver_case, **para_material, **relax}
                myfile.write(json.dumps(parameters, indent = 4, sort_keys = True))
                
def generate_FSI_euler(input_file_folder):
    generate_FSI_euler_aux(input_file_folder, 'long', 1., 100, 5000, macrosteps = 10, rm = 'NEW')
    generate_FSI_euler_aux(input_file_folder, 'verify', 0.001/5, 1, 1, macrosteps = 1, rm = 'NEW')
    generate_FSI_euler_aux(input_file_folder, 'short', 0.1, 10, 500, macrosteps = 1, rm = 'NEW')
    generate_FSI_euler_aux(input_file_folder, 'verify_gs_ref',  0.001/5, 1, 1, macrosteps = 1, rm = 'GS')
    generate_FSI_euler_aux(input_file_folder, 'gs_ref', 0.3, 30, 1500, macrosteps = 3, rm = 'GS')
                
def generate_FSI_euler_aux(input_file_folder, name, tend, n_steps_heat, n_steps_euler, macrosteps = 1, rm = 'GS'):
    from heat_relax_aux import get_parameters, get_relax_parameters
    ## file setup
    file_setup = {'folder': 'heat_DN', 'exe': 'euler_DN'}
    ## problem setup
    prob_setup = {'tend': tend}
    
    ## WR setup
    if rm == 'GS':
        WR_setup = {'nsteps_conv_check': 1, ## steps below tolerance for convergence check
                    'runmodes': ['GS2', 'JAC'],
                    'run_names': ['GS_ND', 'JAC'],
                    'ref_run_name': 'GS_ND',
                    'labels': ['GS_DN', 'JAC'],
                    'maxiter' : 10,
                    'errlog': 1,
                    'errorlogging': 1,
                    'times': 1
                    }
    elif rm == 'NEW':
        WR_setup = {'nsteps_conv_check': 1, ## steps below tolerance for convergence check
                    'times_only_new': 1, ## runtime averaging only for new method
                    'var_relax': 2,
                    'runmodes': ['NEW'],
                    'run_names': ['NEW'],
                    'ref_run_name': 'NEW',
                    'labels': ['NEW'],
                    'maxiter' : 10,
                    'errlog': 1,
                    'errorlogging': 1,
                    'times': 1
                    }
    
    ## test cases setup
    test_case_setup = {'tolerances': [10**(-i) for i in range(11)],
                       'macrosteps': macrosteps}
    
    solver_setup = {'heatsolver': 0} # fenics, nonlinear
    
    basic_setup = {**file_setup, **prob_setup, **WR_setup, **test_case_setup, **solver_setup}
    
    ## solver setup and options
    ## gridsize is hardcoded in euler grid-file (triangle.dgf)
    ## gridsize in heat problem is computed from the here chosen gridsize parameter
    solver_setup = {'timesteps1': n_steps_euler, 'timesteps2': n_steps_heat, 'gridsize': 159}
    
    ## materials and relaxation
    para_material = get_parameters('air_steel')
    relax = get_relax_parameters(**para_material, **solver_setup, **prob_setup)
    
    with open(input_file_folder + '/euler_DN_{}'.format(name), 'w') as myfile:
        parameters = {**basic_setup, **solver_setup, **para_material, **relax}
        myfile.write(json.dumps(parameters, indent = 4, sort_keys = True))
                
def generate_toy_bigger_input_files(input_file_folder):
    ## file setup
    file_setup_toy = {'folder': 'toy', 'exe': 'TOY'}
    file_setup_bigger = {'folder': 'toy', 'exe': 'BIGGER'}
    
    ## problem setup
    prob_setup = {'tend': 2}
    
    ## WR setup
    WR_setup = {'times_only_new': 1, ## runtime averaging only for new method
                'nsteps_conv_check': 3, ## steps below tolerance for convergence check
                'var_relax': 0,
                'runmodes': ['GS1', 'GS2', 'JAC', 'NEW'],
                'run_names': ['GS1', 'GS2', 'JAC', 'NEW'],
                'ref_run_name': 'GS1',
                'labels': ['GS1', 'GS2', 'JAC', 'NEW'],
                'maxiter' : 100,
                'errlog': 1,
                'errorlogging': 1,
                'times': 10
                }
    
    ## test cases setup
    test_case_setup = {'tolerances': [10**(-i) for i in range(9)],
                       'macrosteps': 1}
    
    basic_setup = {**prob_setup, **WR_setup, **test_case_setup}
    
    ## solver setup and options
    solver_setup = {'timesteps': 100} ## functionality testing 
    
    name = 'toy'
    with open(input_file_folder + '/' + name, 'w') as myfile:
        parameters = {**basic_setup, **solver_setup, **file_setup_toy}
        myfile.write(json.dumps(parameters, indent = 4, sort_keys = True))
        
    name = 'bigger'
    with open(input_file_folder + '/' + name, 'w') as myfile:
        parameters = {**basic_setup, **solver_setup, **file_setup_bigger}
        myfile.write(json.dumps(parameters, indent = 4, sort_keys = True))
    
        
if __name__ == '__main__':
    import os
    folder = 'input_files'
    try: os.mkdir(folder)
    except: pass
    
    generate_toy_bigger_input_files(folder)
    generate_heat_DN_input_files(folder)
    generate_FSI_euler(folder)
    