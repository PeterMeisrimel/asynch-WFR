#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  1 17:56:31 2021

@author: Peter Meisrimel, Lund University
"""

from run_tolerances import run_tolerances
from postprocessing_cpp_output import process_output_errlog
from produce_plotting_data import produce_plotting_data
import subprocess
import json
import os

"""
Script for successively running coupled euler problem

Issue: 
There is a memory leak somewhere, which prevents running the simulation in a single go.

Diagnosis: 
Not sure where the memory leak is coming from
A "dry run", skipping all computations and returning constant values, does not cause any memory issues.
As such, WR and plotting does not seem to be the issue, but rather the computations, which seems very strange.

Solution: (for now)
Run the simulation on time-windows, such that each fits the memory
Copy over checkpoints of final solutions and load then in as new initial solution each time
"""

def copy_folder(source, dest, i):
    subprocess.call(f"cp -a {source}. {dest}/{i}/", shell = True)
    
def move_output(source, dest, i):
    subprocess.call(f"mv *.out {dest}/{i}/", shell = True)
    subprocess.call(f"mv *.h5 {dest}/{i}/", shell = True)
    subprocess.call(f"mv *.pvd {dest}/{i}/", shell = True)
    subprocess.call(f"mv *.vtu {dest}/{i}/", shell = True)
    
def rename_file(source, dest):
    os.rename(source, dest)
    
if __name__ == '__main__':
    ### the input file that is used successively
    input_file = 'input_files/euler_DN_short'
    print('Input file: ', input_file)
    
    try:
        with open(input_file, 'r') as myfile:
            parameters_load = json.load(myfile)
    except:
        raise ValueError('invalid filename, check spelling')
        
    print('Input file loaded...')
    
    '''
    parameters need to be sorted:
        manadatory inputs for run_tolerances
        list based inputs
        non manadatory, non list based inputs
    '''
    ## mandatory ones
    mandatory_keys = ['tolerances', 'run_names', 'runmodes', 'folder', 'exe', 'run_prefix', 'errorlogging', 'times', 'ref_run_name']
    mandatory_inputs = {key:value for key, value in parameters_load.items() if key in mandatory_keys}
    for k in mandatory_inputs.keys(): parameters_load.pop(k) ## remove them from parameter dictionary
    
    ## list based parameters
    list_parameters = {key:value for key, value in parameters_load.items() if type(value) is list}
    for k in list_parameters.keys(): parameters_load.pop(k)
    
    ## non manadatory, non list based inputs = whatever is left
    
    heat_path = "../src/heat_DN/heat_solver/python/fenics/"
    euler_path = "../src/heat_DN/euler_solver/python/dune/"
    
    try:
        os.mkdir('ouput')
    except:
        pass
    
    storage_folder = "output/euler"
    try:
        os.mkdir(storage_folder)
    except:
        pass
    
    ### clear up initial solutions
    subprocess.call(f"rm {euler_path}u0_*", shell = True) ## remove potentially old euler backups
    subprocess.call(f"cp {euler_path}u0.out {euler_path}u0_euler_start.out", shell = True) ## set initial one
    
    subprocess.call(f"rm {heat_path}u0_*", shell = True) ## remove potentially old heat u0 backups
    subprocess.call(f"cp {heat_path}u0.h5 {heat_path}u0_heat_start.h5", shell = True) ## set initial one
    
    
    cropped_name = input_file[input_file.find('/') + 1:] ## skipping input_file folder
    for i in range(50): ## number of time-windows used, needs to be chosen in combination with t_end as input parameter
        path = run_tolerances(name = cropped_name, **mandatory_inputs, parameters = parameters_load, **list_parameters)
        print('...processing output')
        process_output_errlog(path)
        print('...producing plotting data')
        produce_plotting_data(path)
        
        try:
            os.mkdir(f"{storage_folder}/{i}")
        except:
            pass
        
        ## store previous starting files
        try:
            rename_file(heat_path + "u0_heat_start.h5", heat_path + f"u0_heat_start_{i}.h5")
        except: 
            print('renaming heat starting file failed')
            pass
        rename_file(euler_path + "u0_euler_start.out", euler_path + f"u0_euler_start_{i}.out")
        
        ## copy over new starting files
        rename_file("fenics_heat_sol_final.h5", heat_path + "u0_heat_start.h5")
        rename_file("euler_final.out", euler_path + "u0_euler_start.out")
        
        ## move all output files to seperate folders
        copy_folder(path, storage_folder, i)
        move_output(path, storage_folder, i)
        
        ## copy over final solutions
        subprocess.call(f"cp {storage_folder}/{i}/heat_fenics_sol_final000000.vtu {storage_folder}", shell = True)
        os.rename(f"{storage_folder}/heat_fenics_sol_final000000.vtu", f"{storage_folder}/heat_fenics_sol_final{i}.vtu")
        subprocess.call(f"cp {storage_folder}/{i}/euler_sol_final_plotting00000.vtu {storage_folder}", shell = True)
        os.rename(f"{storage_folder}/euler_sol_final_plotting00000.vtu", f"{storage_folder}/euler_sol_final_plotting{i}.vtu")
        
        ## manually create pvd?