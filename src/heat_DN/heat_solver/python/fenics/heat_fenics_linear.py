#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 22:31:31 2020

@author: Peter Meisrimel
"""

import dolfin as dol
import numpy as np
from heat_fenics import Problem_heat, Custom_Expr

class Problem_heat_D(Problem_heat):
    ## Dirichlet Problem, boundary to the right, compute flux via weak form
    def __init__(self, gridsize, alpha, lambda_diff, order = 2, xa = -1, xb = 1):
        self.f_old, self.f_new = dol.Constant(0.), dol.Constant(0.)
        ## required for initial reset, values do not matter
        self.flux0, self.flux_old = np.array([0]), np.array([0]) ## need to be set for the resetting
        super(Problem_heat_D, self).__init__(gridsize, alpha, lambda_diff, order, xa = xa, xb = xb, mono = False, xa_grid = xa, xb_grid = 0)
        
        self.n = dol.Constant((1.0, 0.0))
        self.F_flux_grad = (self.lam*dol.dot(dol.grad(self.unew), self.n)*self.vtest*dol.ds)
        self.flux_f = dol.Function(self.V)
        
        if order == 1:
            self.F_flux = (self.a*(self.unew - self.uold)*self.vtest/self.dt*dol.dx + 
                           self.lam*dol.dot(dol.grad(self.unew), dol.grad(self.vtest))*dol.dx)
        elif order == 2:
            self.F_flux = (self.a*(self.unew - self.uold)*self.vtest/self.dt*dol.dx + 
                           0.5*self.lam*dol.dot(dol.grad(self.unew + self.uold), dol.grad(self.vtest))*dol.dx)
            
        ## mark zero boundaries
        def boundary_zero(x, on_boundary):
            return on_boundary and (dol.near(x[0], xa) or dol.near(x[1], self.ya) or dol.near(x[1], self.yb))
        ## marking interface
        def boundary_gamma(x, on_boundary):
            return on_boundary and dol.near(x[0], 0.)
        self.ugamma = Custom_Expr() ## for discrete non-zero Dirichlet boundary data
        self.bcs = [dol.DirichletBC(self.V, dol.Constant(0.), boundary_zero),
                    dol.DirichletBC(self.V, self.ugamma, boundary_gamma)]
            
        ## these are called such that any "just-in-time" compilations are done ahead of time and do not affect any runtime measurements
        self.create_checkpoint()
        self.do_step(0, 1, np.zeros(self.NN))
        self.reset()
            
    def get_u0(self):
        ## compute flux via gradient 
        F_flux0 = dol.assemble(self.lam*dol.dot(dol.grad(self.unew), self.n)*self.vtest*dol.ds)
        self.flux_f.vector().set_local(F_flux0)
        self.flux0 = self.get_u_gamma(self.flux_f)/self.dx
        self.flux_old = np.copy(self.flux0)
        return self.flux_old
            
    def get_flux(self):
        flux_sol = dol.assemble(self.F_flux)
        self.flux_f.vector().set_local(flux_sol)
        if self.order == 1:
            flux_new = self.get_u_gamma(self.flux_f)/self.dx
        elif self.order == 2:
            flux_new = 2*self.get_u_gamma(self.flux_f)/self.dx - self.flux_old
        flux_new[0], flux_new[-1] = 0, 0
        self.flux_old = flux_new
        return flux_new
    
    def do_step(self, t, dt, ug):
        self.dt.assign(dt)
        self.ugamma.update_vals(self.yy, ug) ## set boundary data
        dol.solve(self.lhs == self.rhs, self.unew, self.bcs)
        flux = self.get_flux() # potentially based on uold & unew
        self.uold.assign(self.unew)
        return flux
    
    def reset(self):
        super(Problem_heat_D, self).reset()
        self.flux_old = np.copy(self.flux0)
        
    def create_checkpoint(self):
        super(Problem_heat_D, self).create_checkpoint()
        self.flux0 = np.copy(self.flux_old)
        
    def solve(self, tf, n_steps):
        ## "monolithic solve" using exact reference data at the interface
        self.get_u0() ## for Dirichlet only verification
        dt = tf/n_steps
        for i, t in enumerate(np.linspace(0, tf, n_steps + 1)[:-1]):
            self.get_ex_sol(t + dt) # get solution for u_gamma into self.unew
            self.do_step(t, dt, self.get_u_gamma(self.unew))
    
class Problem_heat_N(Problem_heat):
    ## Neumann Problem, boundary to the left
    def __init__(self, gridsize, alpha, lambda_diff, order = 2, xa = -1, xb = 1):
        ## fluxes are now interpolated from constant data
        self.f_old, self.f_new = Custom_Expr(), Custom_Expr()
        super(Problem_heat_N, self).__init__(gridsize, alpha, lambda_diff, order, xa = xa, xb = xb, mono = False, xa_grid = 0, xb_grid = xb)
        
        ## mark zero boundaries
        def boundary_zero(x, on_boundary):
            return on_boundary and (dol.near(x[0], xb) or dol.near(x[1], self.ya) or dol.near(x[1], self.yb))
        self.bc_D = [dol.DirichletBC(self.V, dol.Constant(0.), boundary_zero)]
        ## Neumann boundaries are the default, need not be marked
        
        self.create_checkpoint()
        self.do_step(0, 1, np.zeros(self.NN), np.zeros(self.NN))
        self.reset()

    def get_u0(self): 
        return self.get_u_gamma(self.unew)
    
    def do_step(self, t, dt, flux_1, flux_2):
        ## flux_1 = flux at t = t
        ## flux_2 = flux at t = t + dt
        self.dt.assign(dt)
        ## update fluxes
        self.f_old.update_vals(self.yy, flux_1)
        self.f_new.update_vals(self.yy, flux_2)

        dol.solve(self.lhs == self.rhs, self.unew, self.bc_D)
        self.uold.assign(self.unew)
        return self.get_u_gamma(self.unew)
    
    def solve(self, tf, n_steps):
        ## "monolithic solve" using exact reference fluxes at the interface
        dt = tf/n_steps
        for i, t in enumerate(np.linspace(0, tf, n_steps + 1)[:-1]):
            self.get_ex_flux(t) # get solution for flux into self.unew
            flux_old = np.copy(self.get_u_gamma(self.unew))
            
            self.get_ex_flux(t + dt) # get solution for flux into self.unew
            flux_new = np.copy(self.get_u_gamma(self.unew))
            
            self.do_step(t, dt, flux_old, flux_new)
            
if __name__ == '__main__':
    import pylab as pl
    pl.close('all')
    
    from heat_fenics import get_solve_WR
    from verification import get_parameters, verify_with_monolithic, verify_comb_error, verify_comb_error_space, plot_theta, verify_self_time, WR_full_error
    from verification_D_N import verify_time, verify_space
    
    savefig = 'heat_DN_'
    solver = get_solve_WR(Problem_heat_D, Problem_heat_N)

    ## verify dirichlet and neumann solvers on their own
    ## for refinement in time
    pp = {'tf': 1., **get_parameters(), 'gridsize': 64, 'xa': -1, 'xb': 1}
    verify_time(Problem_heat_D, True, k = 8, order = 1, **pp, savefig = savefig)
    verify_time(Problem_heat_D, True, k = 8, order = 2, **pp, savefig = savefig)
    
    ## for refinement in space
    pp = {'tf': 1., **get_parameters(), 'xa': -1, 'xb': 1}
    verify_space(Problem_heat_D, True, k = 8, order = 1, N_steps = 100, **pp, savefig = savefig)
    verify_space(Problem_heat_D, True, k = 8, order = 2, N_steps = 100, **pp, savefig = savefig)
    
    pp = {'tf': 1., **get_parameters(), 'gridsize': 64, 'xa': -1, 'xb': 1, 'theta': 0.5}
    ## verify WR converges against monolithic solution
    verify_with_monolithic(solve_WR = solver, k = 8, order = 1, **pp, savefig = savefig)
    verify_with_monolithic(solve_WR = solver, k = 8, order = 2, **pp, savefig = savefig)
    
    ## verify combined error, splitting + time int for decreasing dt
    verify_comb_error(solve_WR = solver, k = 8, order = 1, **pp, savefig = savefig)
    verify_comb_error(solve_WR = solver, k = 8, order = 2, **pp, savefig = savefig)
    
    ## verify combined error, splitting + time int for decreasing dx
    pp = {'tf': 1., **get_parameters(), 'xa': -1, 'xb': 1, 'theta': 0.5}
    verify_comb_error_space(solve_WR = solver, k = 8, order = 1, **pp, savefig = savefig, TOL = 1e-10, N_steps = 100)
    verify_comb_error_space(solve_WR = solver, k = 8, order = 2, **pp, savefig = savefig, TOL = 1e-10, N_steps = 100)
    
    ## verify convergence rate
    pp = {'tf': 1., **get_parameters(), 'gridsize': 64, 'xa': -1, 'xb': 1}
    plot_theta(solve_WR = solver, savefig = savefig, order = 1, **pp)
    plot_theta(solve_WR = solver, savefig = savefig, order = 2, **pp)
    
    ## verify time-integration order with itself
    pp = {'tf': 1., **get_parameters(), 'gridsize': 64, 'xa': -1, 'xb': 1, 'theta': 0.5}
    verify_self_time(solve_WR = solver, savefig = savefig, k = 6, order = 1, **pp)
    verify_self_time(solve_WR = solver, savefig = savefig, k = 6, order = 2, **pp)

    pp = {'tf': 1., **get_parameters(), 'gridsize': 256, 'xa': -1, 'xb': 1, 'theta': 0.5}
    WR_full_error(solve_WR = get_solve_WR(Problem_heat_D, Problem_heat_N),
                      k = 10, order = 2, **pp, savefig = '256_weak_')