#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  9 12:20:55 2021

@author: Peter Meisrimel, Lund University
"""

import numpy as np
import pylab as pl
import dolfin as dol
from heat_fenics import Custom_Expr
#from heat_fenics_grad import Problem_heat_N_euler as parent_class
from mpi4py import MPI

## nonlinear part based on https://fenicsproject.org/pub/tutorial/html/._ftut1007.html
#class Problem_heat_N_euler(parent_class):
class Problem_heat_N_euler:
    u0_filename = "u0_heat_start.h5"
    def __init__(self, gridsize, a = 0, lam = 0, material = 'steel', temp0 = 900):
        ## a, lam are dummy variables for initializing class from C++, can be solved more elegantly
        dol.set_log_level(30) ## supress excess output
        import os
        abs_path = os.getcwd() # get path from where script is called
        ii = abs_path.find("asynch-WFR") ## needs to be within asych-WFR folder structure
        self.loading_prefix = abs_path[:ii+11] + "src/heat_DN/heat_solver/python/fenics/" 
        
        ## coordinates
        ## setting them properly (not shifted/normalized) is required for proper paraview plots
        self.ya, self.yb, self.xa, self.xb = -0.02, 0, 0.1, 0.3
        self.dt = dol.Constant(1.)
        
        ## material parameters
        if material is "steel":
            ## taken from "Numerical Methods for Unsteady Thermal Fluid Structure Interaction" Birken, Monge
            self.lambda_diff = lambda T: 40.1 + 0.05*T - 0.0001*T**2 + 4.9e-8*T**3
            self.cp1 = lambda T: 34.2*dol.exp(0.0026*T) + 421.15
            self.cp2 = lambda T: 956.5*dol.exp(-0.012*(T - 900)) + 0.45*T
            self.cp = lambda T: -10*dol.ln(0.5*(dol.exp(-self.cp1(T)/10) + dol.exp(-self.cp2(T)/10)))
            self.rho = 7836
            self.a = lambda T: self.rho*self.cp(T)
        else:
            raise KeyError('material not implemented')
            
        ## grid
        self.mesh = dol.RectangleMesh(MPI.COMM_SELF,
                                      dol.Point((self.xa, self.ya)),
                                      dol.Point((self.xb, self.yb)),
                                      gridsize + 1, (gridsize + 1)//10)
        self.V = dol.FunctionSpace(self.mesh, "CG", 1) ## function space
        
        ## mark top boundary, since it receives a different treatment
        class Top_boundary(dol.SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and dol.near(x[1], 0.0) ## position hardcoded
        
        ## set self.ds for upper, non-zero flux boundary
        self.neumann_boundary = Top_boundary()
        self.bound_marker = dol.MeshFunction('size_t', self.mesh, 1, 0)
        self.neumann_boundary.mark(self.bound_marker, 1)
        self.ds = dol.Measure('ds', domain = self.mesh, subdomain_data = self.bound_marker)
        
        ## try to read u0 from file, if there is any
        try:
            print('trying to read heat from file', self.loading_prefix + self.u0_filename)
            input_file = dol.HDF5File(MPI.COMM_SELF, self.loading_prefix + self.u0_filename, "r")
            self.u0 = dol.Function(self.V)
            input_file.read(self.u0, '\f')
            input_file.close()
            print('successfully read heat initial data')
        except: ## else, start from constant value
            self.u0 = dol.interpolate(dol.Constant(temp0), self.V)
            print('heat reading from file failed, constant initial value instead')
        
        ## fluxes come from discrete data
        self.f_old, self.f_new = Custom_Expr(), Custom_Expr()
        
        self.vtest = dol.TestFunction(self.V)
        self.uold, self.unew = dol.Function(self.V), dol.Function(self.V)
        self.uold.interpolate(self.u0); self.unew.interpolate(self.u0)
        
        ## weak form, Crank-Nicolson
        self.F = (self.a(self.unew)*(self.unew - self.uold)*self.vtest*dol.dx
                  + 0.5*self.dt*self.lambda_diff(self.uold)*dol.dot(dol.grad(self.uold), dol.grad(self.vtest))*dol.dx
                  + 0.5*self.dt*self.lambda_diff(self.unew)*dol.dot(dol.grad(self.unew), dol.grad(self.vtest))*dol.dx
                  + 0.5*self.dt*(self.f_old + self.f_new)*self.vtest*self.ds(1))
        
        ## coordinates for boundary samples/flux
        self.NN = gridsize + 2
        self.xx = np.linspace(self.xa, self.xb, self.NN) ## for sampling interface
        self.yy = np.linspace(self.ya, self.yb, self.NN) ## meshpoints for interface values
        self.bc_D = []
        
        ## plotting if called via do_step
        self.plotting_dt = 1e-1
        self.plot_count = 1
        
        self.create_checkpoint()
        
        self.plot_file_step = dol.File(MPI.COMM_SELF, "heat_fenics_sol_step.pvd")
        self.plot_file_iter = dol.File(MPI.COMM_SELF, "heat_fenics_sol_iter.pvd")
        self.plot_file_window = dol.File(MPI.COMM_SELF, "heat_fenics_sol_window.pvd")
        self.plot_file_final = dol.File(MPI.COMM_SELF, "heat_fenics_sol_final.pvd")
        
        self.do_step(0, 1, np.zeros(self.NN), np.zeros(self.NN))
        self.reset()
        self.iteration_counter = 0
        
    def create_checkpoint(self):
        ## store current state
        self.u_checkpoint = self.uold.copy()
        
    def reset(self):
        self.plot_count = 1
        ## restore previous states
        self.uold.vector()[:] = self.u_checkpoint.vector()
        self.unew.vector()[:] = self.u_checkpoint.vector()
        self.dt.assign(1.)
        
    def get_u_gamma(self, ugamma):
        return np.array([ugamma(x, self.yb) for x in self.xx])
    
    def do_step(self, t, dt, flux_1, flux_2):
        self.dt.assign(dt)
        ## update fluxes
        self.f_old.update_vals(self.yy, flux_1)
        self.f_new.update_vals(self.yy, flux_2)

        dol.solve(self.F == 0, self.unew, self.bc_D)
        self.uold.assign(self.unew)
        return self.get_u_gamma(self.unew)
    
    def solve(self, tf, n_steps):
        dt = tf/n_steps
        flux = np.ones(self.NN)*10000
        for i, t in enumerate(np.linspace(0, tf, n_steps + 1)[:-1]):
            self.do_step(t, dt, flux, flux)
            
    def callback_finalize(self):
        output_file = dol.HDF5File(MPI.COMM_SELF, "fenics_heat_sol_final.h5", "w")
        output_file.write(self.unew, '\f')
        output_file.close()
        self.plot_file_final << self.unew
        
    def callback_iteration(self):
        self.iteration_counter += 1
        output_file = dol.HDF5File(MPI.COMM_SELF, "fenics_heat_sol_checkpoint_{}.h5".format(self.iteration_counter), "w")
        output_file.write(self.unew, '\f')
        output_file.close()
        self.plot_file_iter << self.unew
    
    def callback_window(self):
        self.iteration_counter += 100
        output_file = dol.HDF5File(MPI.COMM_SELF, "fenics_heat_sol_checkpoint_{}.h5".format(self.iteration_counter), "w")
        output_file.write(self.unew, '\f')
        output_file.close()
        self.plot_file_window << self.unew
        
if __name__ == "__main__":
    pl.close('all')
    p = Problem_heat_N_euler(100)
    p.solve(10, 100)
    pl.figure()
    c = dol.plot(p.unew)
    pl.colorbar(c)