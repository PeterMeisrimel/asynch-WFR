#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 22:31:31 2020

@author: Peter Meisrimel
"""
import numpy as np
import dolfin as dol
from scipy.interpolate import interp1d
from mpi4py import MPI

## this is the primary adapter function for interpolation to FEniCs discrete functions
class Custom_Expr(dol.UserExpression):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
    def update_vals(self, grid, values):
        ## define 1D interpolant
        self.interpolated_data = interp1d(grid, values)
    def eval(self, value, x):
        ## value = output, x = positon
        value[0] = self.interpolated_data(x[1])
    def value_shape(self):
        return ()

## parent class for (linear heat equations)
class Problem_heat:
    def __init__(self, gridsize, alpha, lambda_diff, order = 2, xa = -1., xb = 1., ya = 0., yb = 1., mono = True,
                 xa_grid = None, xb_grid = None):
        ## mono = monolithic solve
        
        dol.set_log_level(30) # supress non critical ouput
        ## grid: [xa_grid, xb_grid] x [0, y]
        ## taking xa_grid = xa, if not set, same for xb
        ## initial condition and reference solution set on xa, xb
        xa, xb, ya, yb = float(xa), float(xb), float(ya), float(yb)
        if xa_grid is None: xa_grid = xa
        if xb_grid is None: xb_grid = xb
        
        self.xa, self.xb = xa, xb
        self.xa_grid, self.xb_grid = xa_grid, xb_grid
        self.ya, self.yb = ya, yb
        self.len, self.len_grid = xb - xa, int(xb_grid - xa_grid)
        
        ## n = gridsize = number of internal unknowns
        ## => n + 2 nodes per unit length
        ## => n + 1 cells per unit length
        self.NN = gridsize + 2
        self.yy = np.linspace(ya, yb, self.NN) ## meshpoints for interface values
        self.dx = 1./(gridsize + 1)
        
        self.order = order ## time-integration
        self.a, self.lam = dol.Constant(alpha), dol.Constant(lambda_diff) ## linear heat eq parameters
        self.dt = dol.Constant(1.) ## needs to be initialized
        
        ## domain shapes for basic heat - heat coupling
        self.mesh = dol.RectangleMesh(MPI.COMM_SELF,
                                      dol.Point((self.xa_grid, self.ya)),
                                      dol.Point((self.xb_grid, self.yb)),
                                      self.len_grid*(gridsize + 1), gridsize + 1)
        self.V = dol.FunctionSpace(self.mesh, "CG", 1)
        
        self.u0_expr = "500*sin((x[0] - {})*M_PI/{})*sin(M_PI*x[1])".format(self.xa, self.len)
        self.u0 = dol.interpolate(dol.Expression(self.u0_expr, degree = 2, mpi_comm = MPI.COMM_SELF), self.V)
        
        ## reference flux, only used for some tests
        self.f0_expr = "{}*500*cos((x[0] - {})*M_PI/{})*sin(M_PI*x[1])*M_PI/{}".format(lambda_diff, self.xa, self.len, self.len)


        ## define trial + test functions and initialize
        self.u = dol.TrialFunction(self.V)
        self.vtest = dol.TestFunction(self.V)
        self.uold, self.unew = dol.Function(self.V), dol.Function(self.V)
        self.uold.interpolate(self.u0); self.unew.interpolate(self.u0)
        
        ## monolithic solve, f terms not required, set to zero
        if mono:
            self.f_old, self.f_new = dol.Constant(0.), dol.Constant(0.)
        
        
        if order == 1: ## Implicit euler method
            self.F = (self.a*(self.u - self.uold)*self.vtest*dol.dx
                      + self.dt*self.lam*dol.dot(dol.grad(self.u), dol.grad(self.vtest))*dol.dx
                      + self.dt*self.f_new*self.vtest*dol.ds)
        elif order == 2: ## Crank-Nicolson method
            self.F = (self.a*(self.u - self.uold)*self.vtest*dol.dx
                      + 0.5*self.dt*self.lam*dol.dot(dol.grad(self.uold + self.u), dol.grad(self.vtest))*dol.dx
                      + 0.5*self.dt*(self.f_old + self.f_new)*self.vtest*dol.ds)
        ## extract right (left)-hand side required when solving
        self.lhs, self.rhs = dol.lhs(self.F), dol.rhs(self.F)
        
        ## monolithic case, all zero Dirichlet-boundaries
        if mono:
            ## marking zero-boundaries
            def boundary_zero(x, on_boundary):
                return on_boundary and (dol.near(x[0], self.xa_grid) or dol.near(x[0], self.xb_grid) 
                                        or dol.near(x[1], self.ya) or dol.near(x[1], self.yb))
            self.bc_D = dol.DirichletBC(self.V, dol.Constant(0.), boundary_zero)
            
        self.create_checkpoint()
        self.reset()
        
    def create_checkpoint(self):
        ## store current state
        self.u_checkpoint = self.uold.copy()
        
    def reset(self):
        ## restore previous states
        self.uold.vector()[:] = self.u_checkpoint.vector()
        self.unew.vector()[:] = self.u_checkpoint.vector()
        self.dt.assign(1.)
    
    ## to be overwritten in child classes
    def callback_iteration(self):
        pass
    def callback_window(self):
        pass
    def callback_finalize(self):
        pass
        
    ## key adapter part, sample solution at interface
    def get_u_gamma(self, ugamma):
        return np.array([ugamma(0., y) for y in self.yy])
    
    ## solve monolithic problem
    def solve(self, tf, n_steps):
        self.dt.assign(tf/n_steps)
        for _ in range(n_steps):
            dol.solve(self.lhs == self.rhs, self.unew, self.bc_D)
            self.uold.assign(self.unew)
    
    ## extract whole discrete solution, requires lots of sampling for the solution
    ## there should be a better way to do ths?
    def get_sol(self, Nx, Ny, dx, xx = 0, offset = 0):
        res = np.zeros(Nx*Ny)
        for i in range(offset,  Nx):
            for j, y in enumerate(np.linspace(self.ya, self.yb, Ny)):
                res[(i - offset)*Ny + j] = self.unew(xx + i*dx, y)
        return res
    
    ## exact reference solution, monolithic + same parameters
    def get_ex_sol(self, t = 1):
        t_fac = np.exp(-(self.len**2 + 1)/(self.len**2)*np.pi**2*t*float(self.lam)/float(self.a))
        self.unew.interpolate(dol.Expression(self.u0_expr + "*" + str(t_fac), degree = 2))
    
    ## reference flux solution
    def get_ex_flux(self, t = 1):
        t_fac = np.exp(-(self.len**2 + 1)/(self.len**2)*np.pi**2*t*float(self.lam)/float(self.a))
        self.unew.interpolate(dol.Expression(self.f0_expr + "*" + str(t_fac), degree = 2))
     
## basic GS WR, single window
## should probably be replaced test cases using the C++ WR solvers
def get_solve_WR(Problem_heat_D, Problem_heat_N):
    def solve_WR(tf, N1, N2, maxiter, TOL, th, Nx = None, Ny = None, dx = None, **kwargs):
        xa = -1 if 'xa' not in kwargs.keys() else kwargs['xa']
        xb = 1 if 'xb' not in kwargs.keys() else kwargs['xb']
        if 'xb' not in kwargs.keys():
            xb = 1
        if Nx is None: Nx = kwargs['gridsize'] + 1 ## nodes per unit length, - 1
        if Ny is None: Ny = kwargs['gridsize'] + 1 ## nodes per unit length, - 1
        if dx is None: dx = 1./(kwargs['gridsize'] + 1)
        
        pD, pN = Problem_heat_D(**kwargs), Problem_heat_N(**kwargs)
        tt1, tt2 = np.linspace(0, tf, N1 + 1), np.linspace(0, tf, N2 + 1)
        dt1, dt2 = tf/N1, tf/N2
        norm = lambda x: np.linalg.norm(x, 2)*np.sqrt(pN.dx)
        
        ## run Waveform iteration 
        ug0, f0 = pN.get_u0(), pD.get_u0()
        ugold = [np.copy(ug0) for i in range(N2 + 1)]
        ugnew = [np.copy(ug0) for i in range(N2 + 1)]
        flux_WF = [np.copy(f0) for i in range(N1 + 1)]
        
        pD.create_checkpoint(); pN.create_checkpoint()
        updates, rel_tol_fac = [], norm(ug0)
        for j in range(maxiter):
            ugold = [np.copy(uu) for uu in ugnew]
            pD.reset(); pN.reset()
            
            ## Dirichlet
            ug_WF_f = interp1d(tt2, ugold, kind = 'linear', axis = 0, fill_value = 'extrapolate')
            for i, t in enumerate(tt1[:-1]):
                flux_WF[i+1] = pD.do_step(t, dt1, ug_WF_f(t + dt1))
                
            ## Neumann
            flux_WF_f = interp1d(tt1, flux_WF, kind = 'linear', axis = 0, fill_value = 'extrapolate')
            for i, t in enumerate(tt2[:-1]):
                ugnew[i+1] = pN.do_step(t, dt2, flux_WF_f(t), flux_WF_f(t + dt2))
                
            ## relaxation
            tmp = np.copy(ugold[-1])
            for i in range(N2 + 1):
                ugnew[i] = (1-th)*ugold[i] + th*ugnew[i]
            
            updates.append(norm(ugnew[-1] - tmp))
            print(j, ' update = ', updates[-1])
            if updates[-1]/rel_tol_fac < TOL: # STOPPING CRITERIA FOR FIXED POINT ITERATION
                break
        return (pD.get_sol(Nx * abs(xa), Ny + 1, dx, xx = xa),
                pN.get_sol(Nx * abs(xb), Ny + 1, dx, xx = 0, offset = 1),
                ugnew[-1], updates, j+1)
    return solve_WR

## basic tests
if __name__ == '__main__':
    import pylab as pl
    pl.close('all')
    from verification import get_parameters, verify_space_error, verify_mono_time
    savefig = 'mono_'
    ## verify space order of monolithic solution
    verify_space_error(1., k = 6, order = 2, **get_parameters(), xa = -2, xb = 1, savefig = savefig)
    
    ## verify time order with itself
    pp = {'tf': 1., **get_parameters(), 'gridsize': 64, 'xa': -2, 'xb': 1}
    verify_mono_time(k = 7, order = 1, **pp, savefig = savefig)
    verify_mono_time(k = 5, order = 2, **pp, savefig = savefig)