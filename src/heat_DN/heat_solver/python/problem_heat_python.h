/*
Authors: Peter Meisrimel
June 2020
*/

#ifndef PROBLEM_HEAT_PYTHON_ADAPTER_H_
#define PROBLEM_HEAT_PYTHON_ADAPTER_H_

#include "Python.h"
#include "waveform.h"
#include "problem.h"
#include "iostream"
//#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION // disables from warning from outdated API
//#include "numpy/arrayobject.h"

class Problem_heat_python : public WRproblem{
protected:
    PyObject *pClass_obj, *Pdt, *Pt;
    double _dx;
public:
    Problem_heat_python(int, double, double, const char*, const char*);
    void create_checkpoint();
    void reset_to_checkpoint();
    void get_u0(double*);
    double get_norm_factor(){
        return std::sqrt(_dx);
    }
    void init_other(int len_other){
        _length_other = len_other;
    };
    
    void callback_iteration();
    void callback_window();
    void callback_finalize();
};

class Problem_heat_python_D : public Problem_heat_python{
public:
    Problem_heat_python_D(int gridsize, double a, double g,
                        const char* py_file = "", const char* py_class = "Problem_heat_D")
                        :Problem_heat_python(gridsize, a, g, py_file, py_class){};
    void do_step(double, double, double *, Waveform *);
};

class Problem_heat_python_N : public Problem_heat_python{
public:
    Problem_heat_python_N(int gridsize, double a, double g,
                        const char* py_file = "", const char* py_class = "Problem_heat_N")
                        :Problem_heat_python(gridsize, a, g, py_file, py_class){};
    void do_step(double, double, double *, Waveform *);
};

class Problem_heat_python_N_euler : public Problem_heat_python_N{
public:
    Problem_heat_python_N_euler(int gridsize, double a, double g,
                              const char* py_file = "", const char* py_class = "Problem_heat_N_euler")
                              :Problem_heat_python_N(gridsize, a, g, py_file, py_class){};
};

#endif //PROBLEM_HEAT_PYTHON_ADAPTER_H_