/*
Authors: Peter Meisrimel
June 2020
*/

#ifndef PROBLEM_EULER_PYTHON_ADAPTER_CPP_
#define PROBLEM_EULER_PYTHON_ADAPTER_CPP_

#include "Python.h"
#include "problem_euler_python.h"
#include "iostream"
#include "cassert"
#define NO_IMPORT_ARRAY
#define PY_ARRAY_UNIQUE_SYMBOL cool_ARRAY_API
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include "numpy/arrayobject.h"
#include <math.h>  // sqrt

Problem_euler_python::Problem_euler_python(int gridsize, const char* py_file, const char* py_class){
    other_init_done = false;
    _length = gridsize + 2;
    _dx  = 1./(gridsize + 1);
    
    PyRun_SimpleString("import sys; import os");
    // adds suitable path to locate files
    // REQUIRES euler_DN to be executed from within asynch-WFR folder
    PyRun_SimpleString("sys.path.append(os.getcwd()[:os.getcwd().find('asynch-WFR')+11] + 'src/heat_DN/euler_solver/python/dune/')"); 

    // import file, resp. python script
    PyObject *pFile = PyImport_Import(PyUnicode_FromString(py_file));
    if (pFile == NULL)
        std::cout << "could not find file?" << std::endl;
    assert(pFile != NULL);
    
    PyObject *pDict = PyModule_GetDict(pFile);
    // get class name to initialize from dictionary
    PyObject *pClass_name = PyDict_GetItemString(pDict, py_class);
    
    // initialize values for constructor class
    PyObject *pClassCall = PyTuple_New(1);
    PyTuple_SetItem(pClassCall, 0, PyLong_FromLong((long)gridsize));
//    PyTuple_SetItem(pClassCall, 1, PyLong_FromLong((long)2));
//    PyTuple_SetItem(pClassCall, 2, PyLong_FromLong((long)3));
    
    // initialize class object
    pClass_obj = PyObject_CallObject(pClass_name, pClassCall);
}

void Problem_euler_python::create_checkpoint(){
    PyObject_CallMethodObjArgs(pClass_obj, PyUnicode_FromString("create_checkpoint"), NULL);
}

void Problem_euler_python::reset_to_checkpoint(){
    PyObject_CallMethodObjArgs(pClass_obj, PyUnicode_FromString("reset"), NULL);
}

void Problem_euler_python::get_u0(double* u_out){
    PyObject *pOutput = PyObject_CallMethodObjArgs(pClass_obj, PyUnicode_FromString("get_u0"), NULL);
    Py_XINCREF(pOutput);
    PyArrayObject *pOutput_arr = reinterpret_cast<PyArrayObject*>(pOutput);
    Py_XINCREF(pOutput_arr);
    double * u_out_local = reinterpret_cast<double*>(PyArray_DATA(pOutput_arr));
    for(int i = 0; i < _length; i++)
        u_out[i] = u_out_local[i];
    
    Py_XDECREF(pOutput);
    Py_XDECREF(pOutput_arr);
}

void Problem_euler_python::callback_iteration(){
    PyObject_CallMethodObjArgs(pClass_obj, PyUnicode_FromString("callback_iteration"), NULL);
}

void Problem_euler_python::callback_window(){
    PyObject_CallMethodObjArgs(pClass_obj, PyUnicode_FromString("callback_window"), NULL);
}

void Problem_euler_python::callback_finalize(){
    PyObject_CallMethodObjArgs(pClass_obj, PyUnicode_FromString("callback_finalize"), NULL);
}

void Problem_euler_python::do_step(double t, double dt, double *u_out, Waveform *WF_in){
    Pt = PyFloat_FromDouble(t);
    Pdt = PyFloat_FromDouble(dt);
    double a_sdirk2 = 1 - 0.5*sqrt(2);
    
    // evaluate WF_in at fisrst stage time-point
    npy_intp dims[1]{_length_other};
    double *cInput1 = new double[_length_other];
    WF_in->eval(t + a_sdirk2*dt, cInput1);
    PyObject *pInput1 = PyArray_SimpleNewFromData(1, dims, NPY_DOUBLE, reinterpret_cast<void*>(cInput1));
    Py_XINCREF(pInput1);
    
    // evaluate WF_in at second stage time-point
    double *cInput2 = new double[_length_other];
    WF_in->eval(t + dt, cInput2);
    PyObject *pInput2 = PyArray_SimpleNewFromData(1, dims, NPY_DOUBLE, reinterpret_cast<void*>(cInput2));
    Py_XINCREF(pInput2);
    
    PyObject *pOutput = PyObject_CallMethodObjArgs(pClass_obj, PyUnicode_FromString("do_step"), Pt, Pdt, pInput1, pInput2, NULL);
    Py_XINCREF(pOutput);
    PyArrayObject *pOutput_arr = reinterpret_cast<PyArrayObject*>(pOutput);
    Py_XINCREF(pOutput_arr);
    double * u_out_local;
    u_out_local = reinterpret_cast<double*>(PyArray_DATA(pOutput_arr));
    for(int i = 0; i < _length; i++)
        u_out[i] = u_out_local[i];
        
    Py_XDECREF(pInput1);
    Py_XDECREF(pInput2);
    Py_XDECREF(pOutput);
    Py_XDECREF(pOutput_arr);
}

#endif //PROBLEM_EULER_PYTHON_ADAPTER_CPP_