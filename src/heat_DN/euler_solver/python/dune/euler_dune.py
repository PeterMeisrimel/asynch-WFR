"""
Created on Thu Jun 11 14:52:41 2020

modified from dune-fempy demo: 
https://www.dune-project.org/sphinx/content/sphinx/dune-fem/euler_nb.html (May 2020)
resp. https://gitlab.dune-project.org/dune-fem/dune-fem-dg/-/blob/master/pydemo/euler/euler.py
@author: Peter Meisrimel, Lund University, with lots of help from Robert Klöfkorn
"""

## Euler System of Gas Dynamics
import numpy as np
import ufl
from dune.grid import reader
from dune.fem.space import dgonb
from dune.alugrid import aluCubeGrid
from dune.ufl import expression2GF, Constant
from dune.femdg import femDGOperator
from dune.fem.utility import Sampler
from scipy.interpolate import interp1d
import dune.generator.algorithm as algorithm
import os
from dune.grid import Marker
from dune.fem.view import filteredGridView
from dune.fem.space import lagrange as solutionSpace

class Model:
    eps = 1e-12
    def __init__(self):
        self.dim = 2
        ## material parameters, source: Philipps Habilitation
        self.gamma = 1.4
        self.R = 287.058 ## R = cp - cv ## specific gas constant
        
        ## initial conditions
        self.rho0 = 1.225 # kg/m**3
        self.T0 = 293.15 # 20°C
        self.TWall = 900
        
        ## setting up initial condition
        self.Ma0 = 0.8 ## Mach number
        self.c = np.sqrt(self.gamma * self.T0 * self.R) ## speed of sound
        self.vx0 = self.c * self.Ma0
        self.kin0 = 0.5 * self.rho0 * self.vx0**2
        self.rhoE0 = self.rho0 * self.T0 * self.R / (self.gamma - 1)
        self.rhoEWall = self.rho0 * self.TWall * self.R / (self.gamma - 1)
        self.U0 = ufl.as_vector([self.rho0, self.rho0 * self.vx0, 0, self.kin0 + self.rhoE0])
        self.UWall = ufl.as_vector([self.rho0, self.rho0 * self.vx0, 0, self.kin0 + self.rhoEWall])
        
        ## by ID, see triangle.dgf file, 5 = interface
        # given in conservative form
        self.boundary = {
                         1: lambda t, x, U: U, ## right, outflow
                         2: lambda t, x, U: self.U0, ## top, farfield
                         3: lambda t, x, U: self.U0, ## left, inflow
                         4: lambda t, x, U: self.slip_y(U), ## lower left a
                         5: lambda t, x, U: self.fix_temp(U, self.TWall, slip_y = True), ## lower centre, slip
                         6: lambda t, x, U: self.slip_y(U)  ## lower right, slip
                         } 
        
        ## heat flux computation via Sutherlands law
        self.Su = 194 ## sutherland constant
        self.T0 = 273 ## reference temp
        self.lambda0 = 0.0241 ## reference heat conductivity
        self.lambda_fac = self.lambda0 * 1/(self.T0**(3/2))*(self.T0 + self.Su)

    def velo(self, U):
        return ufl.as_vector([U[i]/U[0] for i in range(1, self.dim + 1)])
    def rhoeps(self, U): ## small e, based on conservative variables
        v = self.velo(U)
        kin = ufl.dot(v, v) * 0.5*U[0]
        rE = U[self.dim + 1]
        return rE - kin
    def pressure(self, U):
        return (self.gamma - 1)*self.rhoeps(U)
    def toPrim(self, U):
        return U[0], self.velo(U), self.pressure(U)
    def temp(self, U): # from cons
        # ideal gas law
        rho, _, p = self.toPrim(U)
        return p/(rho*self.R)
    def fix_temp(self, U, T, slip_y = False): ## change temperature by adjusting E
        rho, v, _ = self.toPrim(U)
        ## internal energy
        if type(T) in [float, int]:
            rhoeps = rho * self.R * T / (self.gamma - 1) 
        else:
            rhoeps = rho * self.R * T[0] / (self.gamma - 1)
        kin_energy = 0.5 * rho * ufl.dot(v, v)
        Unew = ufl.as_vector([U[0], U[1], U[2], rhoeps + kin_energy])
        if slip_y: 
            return self.slip_y(Unew)
        return Unew
    def slip_y(self, U):
        kin_loss = 0.5 * U[2] * U[2] / U[0]
        return ufl.as_vector([U[0], U[1], 0, U[3] - kin_loss])
    def thermal_cond(self, T): # determine thermal conductivity
        return self.lambda_fac * T**(1.5)/(T + self.Su)
    def heat_flux(self, T, n):
        return self.thermal_cond(T) * ufl.dot(ufl.grad(T), n)
    
    # interface methods for model
    ## convective flux
    def F_c(self, t, x, U):
        rho, v, p = self.toPrim(U)
        return ufl.as_matrix([
                  [rho*v[0], rho*v[1]],
                  [rho*v[0]*v[0] + p, rho*v[0]*v[1]],
                  [rho*v[0]*v[1], rho*v[1]*v[1] + p],
                  [(U[self.dim + 1] + p)*v[0], (U[self.dim + 1] + p)*v[1]]])
    
    ## methods for limiting, currently not used
    def physical(self, t, x, U):
        rho, _, p = self.toPrim(U)
        return ufl.conditional(rho > 1e-8, ufl.conditional(p > 1e-8, 1, 0), 0)
    def jump(self, t, x, U, V):
        _, _, pL = self.toPrim(U)
        _, _, pR = self.toPrim(V)
        return (pL - pR)/(0.5*(pL + pR))
    
class Problem_euler:
    ## filename for saving/loading of initial conditions
    u0_filename = "u0_euler_start.out"
    ## get some more inputs, e.g. various orders
    def __init__(self, num_points_interface = 159, order_space = 0,
                 create_ref_sol = False, step_plotting = 0.1):
        
        ## method of lines operator for setting up preconditioner, plus usage of dune-fem-dg time-integration
        from dune.fem.operator import molGalerkin as molGalerkin
        class MolOp:
            def __init__(self, form, space, interpol_stage_1 = None, interpol_stage_2 = None):
                self._op = molGalerkin(form, space)
                self.space = space
                
                self.uu = self.space.interpolate([0]*4, name = 'uu')
                
                ## formally required, but not used with constant step-sizes
                self.localTimeStepEstimate = [0]
                
                ## operators for interpolation of discrete data as boundary conditions
                self.interpol_stage_1 = interpol_stage_1
                self.interpol_stage_2 = interpol_stage_2
                
            def jacobian(self, ubar):
                from dune.fem.operator import linear as linearOperator
                return linearOperator(self._op, ubar = ubar).as_numpy
            
            def __call__(self, u, v):
                self._op(u, v)
            
            def stepTime(self, c, dt):
                ## c value from RK Butcher tableau
                
                if c == 0: ## called with 0, 0 at the end of each timestep, no interpolation
                    return
                if c == 1: ## second SDIRK2 stage
                    if self.interpol_stage_2 is not None: self.interpol_stage_2()
                else: ## first SDIRK2 stage
                    if self.interpol_stage_1 is not None: self.interpol_stage_1()
                    
            def applyLimiter(self, u):
                pass
                
        self.Model = Model()
        
        ## finding grid + u0 file, (semi) independant of where this script is being called from
        import os
        abs_path = os.getcwd() # get path from where script is called
        ii = abs_path.find("asynch-WFR") ## the semi-part: needs to be within asych-WFR folder structure
        self.loading_prefix = abs_path[:ii+11] + "src/heat_DN/euler_solver/python/dune/" 
        
        self.domain = (reader.dgf, self.loading_prefix + "triangle.dgf") ## read domain specifications from file
        self.gridView = aluCubeGrid(self.domain, serial = True, dimgrid = 2)
        
        ## marking and refinement of spatial grid, in 2 steps
        def mark(e):
            x = e.geometry.center
            return Marker.refine if x[1] < 0.03 and x[0] > 0.05 else Marker.keep
        self.gridView.hierarchicalGrid.adapt(mark)
        
        def mark2(e):
            x = e.geometry.center
            return Marker.refine if x[1] < 0.02 and x[0] > 0.08 else Marker.keep
        self.gridView.hierarchicalGrid.adapt(mark2)
        
        ## setting up space and discrete solution
        self.space = dgonb(self.gridView, dimRange = 4, order = order_space, codegen = False)
        self.uh = self.space.interpolate(self.Model.U0, name = "solution")
        ## tempatures, automatically derived from uh, i.e., any updates in self.uh also affect the temperatures
        ## used in plotting/reconstruction
        self.temp = self.Model.temp(self.uh)
        
        ## define region of grid relevant to interpolation of discrete interface data
        ## needs to contain all cells adjacent to the interface
        def interface_boundary(element):
            ## eps needs to be tuned a bit in case of using finer grids
            eps = 1e-3
            if element.hasBoundaryIntersections():
                x = element.geometry.center
                return (0.1 - eps <= x[0] <= 0.3 + eps) and (x[1] < 0.001 + eps)
            return False
        
        ## GridView reduced to interface section, as marked above
        self.boundary_view = filteredGridView(self.gridView, interface_boundary, domainId = 1)
        ## discrete function space for temperatures
        self.space_temp = dgonb(self.gridView, dimRange = 1, order = 0, codegen = False)
        ## reduced to interface section
        self.space_temp_filtered = dgonb(self.boundary_view, dimRange = 1, order = 0, codegen = False)
        
        ## discrete solutions for temperatures
        ## used in defining the boundaries and as target for interpolation from discrete data
        self.uh_wall_temp = self.space_temp.interpolate(self.Model.TWall, name = 'temp')
        self.uh_wall_temp_filtered = self.space_temp_filtered.interpolate(self.Model.TWall, name = 'temp_filtered')
        
        ## wa
        self.Model.boundary[5] = lambda t, x, U: self.Model.fix_temp(U, self.uh_wall_temp, slip_y = True)
        
        count = 0
        for e in self.boundary_view.elements:
            count += 1
        print(f"Number of cells for boundary interpolation: {count}")
    
        from dune.fem.function import uflFunction
        
        self.eps = 1e-10
        x = ufl.SpatialCoordinate(ufl.quadrilateral)
        self.bctest = uflFunction(self.gridView, name = 'bctest', order = order_space, 
                                  ufl = ufl.conditional(x[1] < self.eps, 
                                            ## IF LOWER
                                            ufl.conditional(x[0] < 0.1 + self.eps, 
                                                ## IF LOWER LEFT
                                                self.Model.slip_y(self.uh),
                                                ## ELSE LOWER LEFT
                                                ufl.conditional(x[0] > 0.3 + self.eps,
                                                    ## IF LOWER RIGHT
                                                    self.Model.slip_y(self.uh),
                                                    ## ELSE LOWER RIGHT = LOWER CENTER
                                                    self.Model.fix_temp(self.uh, self.uh_wall_temp, slip_y = True)
                                                )
                                            ),
                                            ## ELSE LOWER
                                            ufl.conditional(x[1] > 0.05 - self.eps, 
                                                ## IF UPPER
                                                self.Model.U0,
                                                ## ELSE UPPER
                                                ufl.conditional(x[0] < self.eps,
                                                    ## IF LEFT
                                                    self.Model.U0,
                                                    ## ELSE LEFT = RIGHT
                                                    self.uh
                                                )
                                            )
                                        )
                                )
        

        ## time-integration routines                                                
        from dolfin_dg import CompressibleEulerOperator, DGDirichletBC
        
        self.bo = CompressibleEulerOperator(self.gridView, self.space, DGDirichletBC(ufl.ds, self.bctest))
        
        self.u = ufl.TrialFunction(self.space)
        self.v = ufl.TestFunction(self.space)
        
        self.form = self.bo.generate_fem_formulation(self.u, self.v, ufl.dx, ufl.dS)
        self.mol = MolOp(-self.form, self.space,
                         lambda : self.set_intf_temp_which(which = 1),
                         lambda : self.set_intf_temp_which(which = 2))
        
#        self.operator = femDGOperator(self.Model, self.space, limiter = None)
        from dune.femdg.rk import SDIRK2
        self.stepper = SDIRK2(self.mol)
#        from own_stepper import stepper_SDIRK2
#        self.stepper = stepper_SDIRK2(self.space, self.mol, 
#                                      lambda : self.set_intf_temp_which(which = 1),
#                                      lambda : self.set_intf_temp_which(which = 2))
        
        ## load initial values
        if create_ref_sol:
            self.uh.interpolate(self.Model.U0)
        else:
            self.load_u0_from_file()
            
        ## flux computation
        self.n_q = Constant((0., -1.))
        def interface_mark_flux(e):
            eps = 1e-3
            x = e.geometry.center
            return (0.1 - eps <= x[0] <= 0.3 + eps) and (x[1] < 0.001 + eps)
        self.flux_view = filteredGridView(self.gridView, interface_mark_flux, domainId = 1)
        self.space_flux = solutionSpace(self.flux_view, order = 1)
        self.temp_single = self.space_flux.interpolate(Constant(0.), name = "temp_single")
        self.flux_sol = self.space_flux.interpolate(Constant(0.), name = "flux_sol")
        self.heat_flux = expression2GF(self.flux_view, self.Model.heat_flux(self.temp_single, self.n_q), self.temp_single.space.order)
        
        count = 0
        for e in self.flux_view.elements:
            count += 1
        print(f"Number of cells for flux computation: {count}")
        
        self.NN = num_points_interface + 2
        self.norm_scale_fac = np.sqrt(1/(self.NN - 1))
        self.sample_start, self.sample_stop, self.sample_y = 0.1, 0.3, 0. ## taken from triangle.dgf
        self.sample_range = [[self.sample_start, self.sample_y], [self.sample_stop, self.sample_y]]
        self.sample_points = np.linspace(self.sample_start, self.sample_stop, self.NN) ## bottom boundary line
        
        self.sampler = Sampler(self.heat_flux)
        self.get_flux_sample = lambda : self.sampler.lineSample(*self.sample_range, self.NN)[1]
        
        self.t = 0.
        
        ## plotting if called via do_step
        self.temp_reconstr_space = solutionSpace(self.gridView, order = 1)
        self.temp_reconstr = self.temp_reconstr_space.interpolate(Constant(0.), name = 'temp_reconstr')
        
        self.plotting_dt = step_plotting
        self.plot_count = 1
        self.vtk = self.gridView.sequencedVTK('euler_sol_step_plotting', pointdata={'sol': self.uh, 'T': self.temp_reconstr}, subsampling = 0)
        self.vtk_simple = self.gridView.sequencedVTK('euler_sol_simple', pointdata={'T': self.temp_reconstr}, subsampling = 0)
        self.vtk_iter = self.gridView.sequencedVTK('euler_sol_iteration_plotting', pointdata={'sol': self.uh, 'T': self.temp_reconstr}, subsampling = 0)
        self.vtk_window = self.gridView.sequencedVTK('euler_sol_window_plotting', pointdata={'sol': self.uh, 'T': self.temp_reconstr}, subsampling = 0)
        self.vtk_finalize = self.gridView.sequencedVTK('euler_sol_final_plotting', pointdata={'sol': self.uh, 'T': self.temp_reconstr}, subsampling = 0)
        
        self.vtk_temp_interpol = self.gridView.sequencedVTK('euler_heat_interpol', pointdata={'T': self.uh_wall_temp}, subsampling = 0)
        
        ## reading/writing to/from files
        self.checkpoint_reader = algorithm.load("readData", self.loading_prefix + "dataio.hh", self.uh, "euler_checkpoint.out")
        self.checkpoint_writer = algorithm.load("writeData", self.loading_prefix + "dataio.hh", self.uh, "euler_checkpoint.out")
        
        self.flux_scale_fac = 0.05/10/(2**2) ## height/number of cells + 2 refinement steps
        
        self.iteration = 0 
        self.checkpoint_counter = 0
#        self.init_step()
        self.create_ref_sol = create_ref_sol
        self.t_checkpoint = 0.
        
    def load_u0_from_file(self):
        readData = algorithm.load("readData", self.loading_prefix + "dataio.hh", self.uh, self.u0_filename)
        print('trying to load u0 from', self.loading_prefix + self.u0_filename)
        try:
            readData(self.uh, self.loading_prefix + self.u0_filename)
            print('loading successful!')
        except:
            print('Loading u0 from file failed, using constant initial condition instead')
            self.uh.interpolate(self.Model.U0)
        
    def write_u0_to_file(self):
        writeData = algorithm.load("writeData", self.loading_prefix + "dataio.hh", self.uh, self.u0_filename)
        writeData(self.uh, self.u0_filename)
        
    def create_checkpoint(self):
        if self.checkpoint_counter > 0:
            os.rename("euler_checkpoint.out", "euler_checkpoint_{}.out".format(self.checkpoint_counter))
        self.checkpoint_counter += 1
        self.checkpoint_writer(self.uh, "euler_checkpoint.out")
        self.t_checkpoint = self.t
        self.iteration = 0
        
    def reset(self):
        self.checkpoint_reader(self.uh, "euler_checkpoint.out")
        self.t = self.t_checkpoint
        self.plot_count = 1
        self.iteration += 1
        
        self.precond_update_counter = 0
        
#    def init_step(self, dt = 1e-12):
#        self.create_checkpoint()
#        if dt is not None:
#            self.do_step(0., dt)
#        self.reset()
        
    def get_u0(self):
        self.initial_flux = self.get_flux()
        return self.initial_flux
    
    def callback_iteration(self):
        self.reconstruct_temperatures()
        self.vtk_iter()
    
    def callback_window(self):
        self.checkpoint_counter += self.checkpoint_counter//100 + 100
        self.reconstruct_temperatures()
        self.vtk_window()
    
    def callback_finalize(self):
        self.reconstruct_temperatures()
        self.vtk_finalize()
        self.checkpoint_writer(self.uh, "euler_final.out")
        
    def reconstruct_temperatures(self):
        self.temp_reconstr.interpolate(self.temp)
        
    def get_flux(self):
        self.temp_single.interpolate(self.temp)
        return self.get_flux_sample()/self.flux_scale_fac
    
    def set_intf_temp_which(self, which = 1):
        intp_temp = interp1d(self.sample_points, self.vals1 if which == 1 else self.vals2, fill_value = 'extrapolate')
        self.uh_wall_temp_filtered.interpolate(lambda x: intp_temp(x[0]))
        self.uh_wall_temp.dofVector.assign(self.uh_wall_temp_filtered.dofVector)
        
    def do_step(self, t, dt, vals1, vals2):
        print('euler step', t, dt)
        self.vals1, self.vals2 = vals1, vals2
        self.stepper.update_precond(self.mol.jacobian, self.uh, dt)
        self.t += self.stepper(self.uh, dt)
        if t >= self.plotting_dt * self.plot_count:
            self.reconstruct_temperatures()
            self.vtk_simple()
            self.plot_count += 1
        return self.get_flux()
    
    def solve(self, tf, write = False, sample_time = None, dt = 1.25e-6):
        self.t = 0
        
        st = 1 ## sampling counter
        sample_time = dt if sample_time is None else sample_time
        old_flux = self.get_flux()
        
        ## needs to be initialized
        self.vals1 = self.Model.TWall*np.ones(len(self.sample_points))
        self.vals2 = self.vals1
        
        n = int(np.round(tf/dt))
        
        if write: 
            self.reconstruct_temperatures()
            self.vtk() ## write initial condition
            
        for i in range(n):
            print('doing step no. {} of {} at time {}, dt = {}'.format(i+1, n, self.t, dt), flush = True)
            self.stepper.update_precond(self.mol.jacobian, self.uh, dt)
            self.t += self.stepper(self.uh, dt = dt)
            
            if write and self.t > st*sample_time: 
                new_flux = self.get_flux()
#                print('flux norm', np.linalg.norm(new_flux, 2)*self.norm_scale_fac)
                print('flux update', np.linalg.norm(new_flux - old_flux, 2)*self.norm_scale_fac)
                old_flux = np.copy(new_flux)
                self.reconstruct_temperatures()
                self.vtk()
                st += 1
            if self.t > tf:
                break
    
    def conv_test_solve(self, n, dt):
        self.t = 0
        ## sdirk 2
        a = 1 - 0.5*np.sqrt(2)
        
        ## interface temperatures represent cooling from TWall (900) to 300 K in 10 seconds
        intf_temp = lambda t: np.ones(self.NN)*max(300, (self.Model.TWall - (self.Model.TWall - 300)*t))
        
        for i in range(n):
            print('step ', i+1, 'out of ', n)
            self.do_step(self.t, dt, intf_temp(self.t + a*dt), intf_temp(self.t + dt))
            
        self.temp_single.interpolate(self.temp)
        return self.temp_single.as_numpy.copy()
            
if __name__ == '__main__':
    # creating reference stationary solution
    tf = 1e-2
    solver = Problem_euler(create_ref_sol = True)
    solver.solve(tf, write = True, dt = 1e-5)
    solver.write_u0_to_file()
    
    ## simple run testings
#    tf = 1e-5
#    solver = Problem_euler()
#    n = 1
#    import time
#    tt = time.time()
#    solver.conv_test_solve(n, tf/n)
#    print(time.time() - tt)
    
    ## convergence order test
    ## running the convergence test might require a more coarse grid + according reference solution, e.g., removing the refinements
    ## does not yield a clear 2nd order, but certainly more than first order
#    tf = 1e-3
#    k = 10 ## depth
#    sols = []
#    solver = Problem_euler()
#    solver.create_checkpoint()
#    for i in range(k):
#        print('k = ', i+1, ' out of ', k)
#        solver.reset()
#        steps = 2**i
#        sols.append(solver.conv_test_solve(steps, tf/steps))
#        print(sols[-1])
#        
#    ref_sol = sols[-1]
#    errs = []
#    for sol in sols[:-1]:
#        errs.append(np.linalg.norm(sol - ref_sol, 2))
#        print('errs', errs[-1])
#    
#    for i in range(len(errs) - 1):
#        print(np.log2(errs[i]/errs[i+1]))