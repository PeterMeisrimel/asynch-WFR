#ifndef DUNE_FEMPY_DATAIO_HH
#define DUNE_FEMPY_DATAIO_HH

#include <tuple>
#include <utility>

#include <dune/common/hybridutilities.hh>
#include <dune/fem/solver/timeprovider.hh>
#include <dune/fem/io/streams/binarystreams.hh>

// author: Robert Klöfkorn

template <class DF >
void writeData(const DF& df,
               const std::string& filename )
{
    Dune::Fem::BinaryFileOutStream stream( filename );
    //Dune::Fem::ASCIIOutStream file( filename );
    df.write( stream );
}

template <class DF >
void readData(DF& df,
              const std::string& filename )
{
    Dune::Fem::BinaryFileInStream stream( filename );
    //Dune::Fem::ASCIIOutStream file( filename );
    df.read( stream );
}
#endif
