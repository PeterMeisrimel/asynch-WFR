#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May  8 13:15:33 2021

@author: Peter Meisrimel, Lund University
"""

## based on dune-fem-dg/python/dune/fem-dg/rk.py

import numpy as np
from scipy.sparse.linalg import LinearOperator, spilu
import scipy.sparse as sps
from scipy.optimize import newton_krylov

class stepper_IE():
    ## implicit Euler
    def __init__(self, space, op):
        self.op = op
        self.dt = 1e-10
        self.temp = space.interpolate([0]*4, name = 'temp')
        self.res = space.interpolate([0]*4, name = 'res')
        
    def newton_f(self, x):
        self.temp.as_numpy[:] = x
        self.op(self.temp, self.res)
        return -x + self.uold + self.dt*self.res.as_numpy[:]
    
    def __call__(self, uh, dt = 1e-10):
        self.dt = dt
        
        uh_array = uh.as_numpy
        self.uold = uh_array.copy()
        target = uh_array.copy()
        
        self.update_precond(uh, dt)
        update = newton_krylov(self.newton_f, target,
                              inner_M = None if not hasattr(self, "precond_M") else self.precond_M,
                              verbose=True)
        uh_array[:] = update
        return dt
    
    def update_precond(self, uh, dt):
        self.jac = self.op.jacobian(uh)
        m = sps.csc_matrix(-sps.eye(self.jac.shape[0]) + self.dt*self.jac)
        invdiag = sps.diags(1/m.diagonal())
        self.precond_M = LinearOperator(shape = self.jac.shape, matvec = invdiag.dot)
        return
    
class stepper_SDIRK2():
    a = 1 - 0.5*np.sqrt(2)
    def __init__(self, space, op, callback1 = None, callback2 = None, verbose = False):
        self.op = op
        self.dt = 1e-10
        self.temp = space.interpolate([0]*4, name = 'temp')
        self.res = space.interpolate([0]*4, name = 'res')
        self.k1 = space.interpolate([0]*4, name = 'k1')
        self.callback1 = callback1
        self.callback2 = callback2
        self.verbose = verbose
        
    def newton_f(self, x):
        self.temp.as_numpy[:] = x
        self.op(self.temp, self.res)
        return -x + self.uold + self.a*self.dt*self.res.as_numpy[:]

    ## for producing better initial guesses
    def f_eval(self, x):
        self.temp.as_numpy[:] = x
        self.op(self.temp, self.res)
        return self.res.as_numpy[:].copy()
    
    def __call__(self, uh, dt = 1e-10):
        self.dt = dt
        
        uh_array = uh.as_numpy
        self.uold = uh_array.copy()
        target = uh_array.copy()
        
        self.update_precond(uh, dt)
        
        if self.callback1 is not None: self.callback1()
        U1 = newton_krylov(self.newton_f, target,
                           inner_M = None if not hasattr(self, "precond_M") else self.precond_M, verbose = self.verbose, line_search = None)
        k1_array = self.k1.as_numpy
        k1_array[:] = (U1 - uh_array[:])/(self.a * dt)
        self.uold += (1 - self.a)*dt*k1_array[:]
        
        target = self.uold.copy()
        if self.callback2 is not None: self.callback2()
        U2 = newton_krylov(self.newton_f, target,
                           inner_M = None if not hasattr(self, "precond_M") else self.precond_M, verbose = self.verbose, line_search = None)
        
        uh_array[:] = U2
        return dt
    
    def update_precond(self, uh, dt):
        self.jac = self.op.jacobian(uh)
        m = sps.csc_matrix(-sps.eye(self.jac.shape[0]) + self.a*self.dt*self.jac)
#        invdiag = sps.diags(1/m.diagonal())
#        self.precond_M = LinearOperator(shape = self.jac.shape, matvec = invdiag.dot)
        
        ilu_mat = spilu(m, fill_factor = 50, drop_tol = 1e-12)
        self.precond_M = LinearOperator(shape = self.jac.shape, matvec = ilu_mat.solve)