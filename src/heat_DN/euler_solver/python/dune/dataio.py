import os
# ensure some compilation output for this example
os.environ['DUNE_LOG_LEVEL'] = 'info'
print("Using DUNE_LOG_LEVEL=",os.getenv('DUNE_LOG_LEVEL'))

## example of saving/loading discrete function to/from file
## author: Robert Klöfkorn

from dune.grid import cartesianDomain
from dune.alugrid import aluCubeGrid as leafGridView
from dune.ufl import Constant
from ufl import SpatialCoordinate, sin
from dune.fem.function import integrate, uflFunction
from dune.fem.space import lagrange, dgonb
from dune.fem import parameter
import dune.generator.algorithm as algorithm

# enable some debug output
# fem.parameter.append({"fem.verboserank":0})

domain = cartesianDomain([0, 0], [1, 1], [20, 20])

gridView  = leafGridView(domain, dimgrid = 2)

lagspace = lagrange(gridView, order = 3, storage = "fem")

x = SpatialCoordinate(lagspace)
f = sin( x[0] ) * sin( x[1] )

uh = lagspace.interpolate(f, name = "uh")

uh.plot()

filename = "u0_test.out"
writeData = algorithm.load("writeData", "dataio.hh", uh, filename)

# write checkpoint, creates file 'checkpoint' and data in subdir 000000,
# path can be set with fem.prefix
writeData(uh, filename)

uh.clear()

# checkFile = 'checkpoint'
readData = algorithm.load("readData", "dataio.hh", uh, filename)

# read data again
readData(uh, filename)

uh.plot()
