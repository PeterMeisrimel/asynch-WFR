/*
Authors: Peter Meisrimel
June 2020
*/

#ifndef PROBLEM_EULER_PYTHON_ADAPTER_H_
#define PROBLEM_EULER_PYTHON_ADAPTER_H_

#include "Python.h"
#include "waveform.h"
#include "problem.h"
#include "iostream"
//#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION // disables from warning from outdated API
//#include "numpy/arrayobject.h"

class Problem_euler_python : public WRproblem{
protected:
    PyObject *pClass_obj, *Pt, *Pdt;
    double _dx;
public:
    Problem_euler_python(int gridsize, const char* py_file, const char* py_class = "Problem_euler");
    void create_checkpoint();
    void reset_to_checkpoint();
    void get_u0(double*);
    void finalize();
    void do_step(double, double, double *, Waveform *);
    double get_norm_factor(){
        return std::sqrt(_dx);
    }
    void init_other(int len_other){
        _length_other = len_other;
    };
    
    void callback_iteration();
    void callback_window();
    void callback_finalize();
};

#endif //PROBLEM_EULER_PYTHON_ADAPTER_H_