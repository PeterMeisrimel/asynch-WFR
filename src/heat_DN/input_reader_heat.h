/*
Authors: Peter Meisrimel
June 2019
*/

#ifndef INPUT_READER_HEAT_H_
#define INPUT_READER_HEAT_H_

// process of thermal FSI specific input paramters, such as thermal cond. + diff.

void process_inputs_heat(int, char**, double&, double&, double&, double&, int&, int&, int&, int&, int&);

#endif // INPUT_READER_HEAT_H_
