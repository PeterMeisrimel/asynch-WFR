/*
Authors: Peter Meisrimel
December 2018
*/

#include "WFR_GS.h"
#include "waveform.h"
#include "problem.h"
#include "math.h" // for sqrt
#include "mpi.h"
#include <stdexcept>
#include <iostream>

WFR_GS::WFR_GS(MPI_Comm comm, double t_end, WRproblem * p1, WRproblem * p2, bool first) : WFR_serial(comm){
    _t_end   = t_end;
    prob_self  = p1;
    prob_other = p2;
    FIRST    = first;
    WF_iters = 0;
}

void WFR_GS::run(double WF_TOL, int WF_MAX_ITER, int steps_macro, int steps_self, int steps_other,
                 int conv_check, int nsteps_conv_check, bool errlogging){
    // set up convergence check parameters
    steps_converged = 0;
    steps_converged_required = nsteps_conv_check;
    conv_which = conv_check;

    // get exchange lengthes
    DIM_SELF = prob_self->get_length();
    DIM_OTHER = prob_other->get_length();
    prob_self->init_other(DIM_OTHER);
    prob_other->init_other(DIM_SELF);

    // initial values
    u0_self  = new double[DIM_SELF];
    u0_other = new double[DIM_OTHER];
    prob_self->get_u0(u0_self);
    prob_other->get_u0(u0_other);
    
    // placeholder for convergence check
    WF_self_last  = new double[DIM_SELF];
    WF_other_last = new double[DIM_OTHER];

    // length of waveform in terms of timesteps
    int WF_LEN_SELF  = steps_self/steps_macro + 1;
    int WF_LEN_OTHER = steps_other/steps_macro + 1;

    // initiliaze own waveform
    // times vector
    times_self = new double[WF_LEN_SELF];
    double dt_self = _t_end/steps_self;
    for (int i = 0; i < WF_LEN_SELF; i++)
        times_self[i] = i*dt_self;
    // data vector
    WF_self_data = new double[WF_LEN_SELF * DIM_SELF];
    WF_self      = new Waveform(WF_LEN_SELF, DIM_SELF, times_self, WF_self_data);
    // initialize, setting last since the "full initialization" happens at beginning of each time-window
    WF_self->set_last(u0_self); 
    // auxiliary vector for relaxation
    relax_aux_vec = new double[DIM_SELF];
    
    // initialize other waveform, same as above
    times_other = new double[WF_LEN_OTHER];
    double dt_other = _t_end/steps_other;
    for (int i = 0; i < WF_LEN_OTHER; i++)
        times_other[i] = i*dt_other;

    WF_other_data = new double[WF_LEN_OTHER * DIM_OTHER];
    WF_other      = new Waveform(WF_LEN_OTHER, DIM_OTHER, times_other, WF_other_data);
    WF_other->set_last(u0_other);

    // initialization of error logging
    log_errors = errlogging;
    init_error_log(steps_macro, WF_MAX_ITER);

    double window_length = _t_end/steps_macro; // (time) length of each time-window

    // additional scaling factor for norm computation
    norm_factor = prob_self -> get_norm_factor(); // implicitly assumed to be identical for both subproblems

    runtime = MPI_Wtime(); // runtime measurement start
    for(int i = 0; i < steps_macro; i++){
        prob_self->create_checkpoint();
        prob_other->create_checkpoint();

        // "loop around", old last value = new starting value
        WF_self ->get_last(u0_self);
        WF_self ->set(0, u0_self);

        WF_other->get_last(u0_other);
        WF_other->set(0, u0_other);

        // depending on order, only one waveform needs to be initialized
        if (FIRST){ // first == true, self first, then other
            WF_other->init_by_last();
        }else{
            WF_self->init_by_last();
        }

        get_relative_tol(); // get tolerance for relative update termination check
      
        // actual iteration right here
        do_WF_iter(WF_TOL, WF_MAX_ITER, WF_LEN_SELF - 1, WF_LEN_OTHER - 1);
        
        // time-window done + not last one -> shift times
        if(i != steps_macro - 1){
            WF_self ->time_shift(window_length);
            WF_other->time_shift(window_length);
        }
        prob_self->callback_window();
        prob_other->callback_window();
    }
    runtime = MPI_Wtime() - runtime; // runtime measurement end
    
    prob_self->callback_finalize();
    prob_other->callback_finalize();
}

void WFR_GS::do_WF_iter(double WF_TOL, int WF_MAX_ITER, int steps_per_window_self, int steps_per_window_other){
    first_iter = true; // convergence check is skipped in first iteration, inside actual function call
    steps_converged = 0;
    
    // are these actually necessary here?
    WF_other->init_by_last();
    WF_self->init_by_last();
    
    for(int i = 0; i < WF_MAX_ITER; i++){ // WF iter loop
        WF_iters++;
        
        // integrate the time-window, dependent on order
        if (FIRST){
            integrate_window(WF_self, WF_other, steps_per_window_self, prob_self, theta_relax_self);
            integrate_window(WF_other, WF_self , steps_per_window_other, prob_other, theta_relax_other);
        }else{
            integrate_window(WF_other, WF_self , steps_per_window_other, prob_other, theta_relax_other);
            integrate_window(WF_self, WF_other, steps_per_window_self, prob_self, theta_relax_self);
        }
        
        prob_self->callback_iteration();
        prob_other->callback_iteration();
        
        if (check_convergence(WF_TOL)){
            steps_converged++;
            if (steps_converged >= steps_converged_required) // sufficiently many steps registered convergence, stop
                break;
        }else{
            steps_converged = 0;
        }
        // backup of "old" last value, used in convergence check in the next iteration
        WF_self    ->get_last(WF_self_last);
        WF_other   ->get_last(WF_other_last);
        // possibly reset internal states of a solver
        prob_self  ->reset_to_checkpoint();
        prob_other ->reset_to_checkpoint();
    } // END WF iter loop
}
