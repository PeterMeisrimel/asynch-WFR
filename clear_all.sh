cd src/toy
make clean

cd ../heat_DN
rm heat_solver/cpp/fenics/heat.h
rm heat_solver/cpp/fenics/heat_flux_grad.h
rm heat_solver/cpp/fenics/heat_flux_weak.h
make clean
rm -r CMakeFiles
rm CMakeCache.txt
