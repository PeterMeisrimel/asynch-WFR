# Waveform Relaxation using asynchronous time-integration

This is an C++ implementation of a Waveform Relaxation based coupled problems solver (for exactly two problems). Aside from the classical Jacobi and Gauss-Seidel Waveform Relaxation (WR) algorithms it also includes a new method using asynchronous communication in the time-integration, which is being researched and developed.

## Authors

Peter Meisrimel, Lund University, Sweden, peter.meisrimel@na.lu.se

Robert Klöfkorn, Lund University, Sweden

Benjamin Rüth, Munich University, Germany

Philipp Birken, Lund University, Sweden

## License

Published under the GNU General Public License v3.0 License

## Software requirements

At least MPI 2.x, respectively an implementation of the MPI 3 Standard. Has been developed using Open MPI 2.1.1

For the heat equation test problem: FEniCs 2019.2.0.dev0

DUNE

python-dev (embedding Python in C++)

## Publications

Meisrimel, Peter, and Philipp Birken. "Waveform Iteration with asynchronous communication." PAMM 19.1 (2019)

Meisrimel, Peter, and Philipp Birken. "Waveform Iteration with asynchronous communication." arXiv, https://arxiv.org/abs/2106.13147

## Related Literature

Parallel multigrid waveform relaxation for parabolic problems, Stefan Vandevalle, as a good introduction to WR.

J Janssen, S Vandewalle: 

"On SOR waveform relaxation methods"

"Multigrid waveform relaxation of spatial finite element meshes: the continuous-time case"

"Multigrid waveform relaxation on spatial finite element meshes: the discrete-time case"

## Basic setup and verification of working installation

(Last updated: Mar. 30 2021): To make Dune work properly with MPI, one currently requires the following fixes:

Replace all "MPI_COMM_WORLD" by "MPI_COMM_SELF" in dune-common/dune/common/parallel/mpihelper.hh, communicator.hh, mpicommunication.hh

Comment the two comm.barrier() calls in dune-common/python/dune/generator/builder.py


For usage with Dune, change runtime_state/activate_dune.sh to have the correct absolute path to your dune activation script.

Run compile_all.sh to compile all problems.

In the runtime_stats folder: 

Run "python3 generate_input_files" to generate input files.

Run "sh run_test.sh" to execute basic functionality tests for all problems.

## Test Problems

Included test problems:

toy(bigger) - A coupling of a scalar equations another scalar (2 scalars), developed for verification purposes only.

heat_DN - Coupling of two linear heat problems (finite elemets via FEniCs (C++/Python) or DUNE) via a Dirichlet-Neumann coupling at the interface.

euler_DN - Coupling of Euler equations (DG0 (FV), DUNE) with nonlinear heat equation (FE, FEniCs) via a Dirichlet-Neumann coupling at the interface.

## Useage

A given problem class needs to be an instance of WRproblem (problem.h) and provide the according functions.

In runtime_stats, run "python3 simulate.py input_files/_input_file_" to run a given set of simulations.

More documentation will come soon.

## MPI Issues

FEniCs runs may finish on error messages due to something in MPI_Finalize, which I could not figure out thus far. The root cause appears to be the dolfin expression. Replacing it by a UserExpression does not solve the problem. Possibly, the issue is that it goes back to C in some way to evaluate this? 

See also: https://fenicsproject.discourse.group/t/expression-class-causing-trouble-for-python-embedded-in-c/2851

This can be "fixed" skipping the MPI_Finalize and running with -quiet to hide the resulting error message at the end. However, this will also hide any legit MPI error messages.

Maybe this stuff will be fixed in future FEniCs versions?

## Troubleshooting and common issues

Make sure to activate the suitable enviroments if you want to use the DUNE or FEniCs solvers.

Note that ANY errors on the python level, when embedded in C++, will lead to a segmentation fault. It is recommendable to debug Python code to be embedded on the Python level and then make sure the importing works correctly.

In case of problems with importing (DUNE) modules in parallel, make sure to run dune-common/bin/setup-dunepy.py before starting computations

Calling of run scripts or similar are currently only working if called from within the /asynch-WFR directory. Background: The asynch-WFR folder acts as the root when adding pathes for loading python modules from C++.

## Acknowledgments

Joachim Hein, Lund University, Sweden, for support on many technical questions regarding MPI.

Azahar Monge, DeustoTech, Spain, usage of older code to verify correctness of heat equation example.
