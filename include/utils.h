/*
Authors: Peter Meisrimel
January 2019
*/

#ifndef UTILS_H_
#define UTILS_H_

#include "problem.h"

// processing of input parameters via command line
// only contains those common to all WRproblem types
// input processing for problem specific paramters, e.g., material parameters should be done in the specific problem subfolder
void process_inputs(int argc, char **argv,
                    double& t_end,
                    int& timesteps1, int& timesteps2,
                    int& runmode, int& macrosteps, int& maxiter, double& WF_TOL, bool& error_logging, int& nsteps_conv_check,
                    double &theta_relax1, double &theta_relax2,
//                    bool &var_relax,
                    int &var_relax,
                    double &theta_relax_gs_a_1, double &theta_relax_gs_a_2,
                    double &theta_relax_gs_b_1, double &theta_relax_gs_b_2);

// main "running function" for executing a WR simulation
// processing inputs and setting up the correct datastructures for executing the run
void setup_and_run_WFR(WRproblem * prob1, WRproblem * prob2, int which_conv, double t_end, int timesteps, int argc, char *argv[]);

// least common multiple
int lcm(int, int);
// greated common divisor
int gcd(int, int);

#endif // UTILS_H_
